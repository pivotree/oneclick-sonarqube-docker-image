ARG SONARQUBE_VERSION=8.4.2-community

FROM sonarqube:${SONARQUBE_VERSION}

ENV SONARQUBE_FOLDER=/opt/sonarqube

ENV COMMUNITY_BRANCH_PLUGIN_VERSION=1.5.0
ENV SONAR_JAVA_PLUGIN_VERSION=6.9.0.23563
ENV SONAR_CSS_PLUGIN_VERSION=1.3.1.1642
ENV SONAR_JAVASCRIPT_PLUGIN_VERSION=6.6.0.13923
ENV SONAR_TYPESCRIPT_PLUGIN_VERSION=2.1.0.4359
ENV SONAR_XML_PLUGIN_VERSION=2.0.1.2020
ENV SONAR_JACOCO_PLUGIN_VERSION=1.1.0.898
ENV SONAR_HTML_PLUGIN_VERSION=3.2.0.2082
ENV SONAR_SCM_GIT_PLUGIN_VERSION=1.12.1.2064
ENV SONAR_GO_PLUGIN_VERSION=1.8.0.1775
ENV SONAR_FINDBUGS_PLUGIN_VERSION=4.0.2


USER sonarqube

RUN if [ -e "${SONARQUBE_FOLDER}/extensions/downloads/*" ]; then rm -fr ${SONARQUBE_FOLDER}/extensions/downloads ; fi && \
    mkdir ${SONARQUBE_FOLDER}/extensions/downloads && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://github.com/mc1arke/sonarqube-community-branch-plugin/releases/download/1.5.0/sonarqube-community-branch-plugin-$COMMUNITY_BRANCH_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-java-plugin/sonar-java-plugin-$SONAR_JAVA_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-css-plugin/sonar-css-plugin-$SONAR_CSS_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-javascript-plugin/sonar-javascript-plugin-$SONAR_JAVASCRIPT_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-typescript-plugin/sonar-typescript-plugin-$SONAR_TYPESCRIPT_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-xml-plugin/sonar-xml-plugin-$SONAR_XML_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-jacoco-plugin/sonar-jacoco-plugin-$SONAR_JACOCO_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-html-plugin/sonar-html-plugin-$SONAR_HTML_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-scm-git-plugin/sonar-scm-git-plugin-$SONAR_SCM_GIT_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://binaries.sonarsource.com/Distribution/sonar-go-plugin/sonar-go-plugin-$SONAR_GO_PLUGIN_VERSION.jar" && \
    wget --directory-prefix ${SONARQUBE_FOLDER}/extensions/downloads --no-verbose --no-check-certificate "https://repo.maven.apache.org/maven2/com/github/spotbugs/sonar-findbugs-plugin/$SONAR_FINDBUGS_PLUGIN_VERSION/sonar-findbugs-plugin-$SONAR_FINDBUGS_PLUGIN_VERSION.jar" && \
    cp -f ${SONARQUBE_FOLDER}/extensions/downloads/sonarqube-community-branch-plugin-${COMMUNITY_BRANCH_PLUGIN_VERSION}.jar ${SONARQUBE_FOLDER}/lib/common/sonarqube-community-branch-plugin-${COMMUNITY_BRANCH_PLUGIN_VERSION}.jar